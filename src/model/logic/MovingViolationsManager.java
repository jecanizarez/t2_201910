package model.logic;

import api.IMovingViolationsManager;
import model.vo.VOMovingViolations;
import model.data_structures.LinkedList;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import com.opencsv.*;

public class MovingViolationsManager implements IMovingViolationsManager {


	private LinkedList<VOMovingViolations> lista;
	public void loadMovingViolations(String movingViolationsFile){
		// TODO Auto-generated method stub
          lista = new LinkedList<VOMovingViolations>();

		try 
		{

			CSVReader lector = new CSVReader(new FileReader(movingViolationsFile));
			String[] linea = lector.readNext();
			linea = lector.readNext();
			while(linea != null)
			{
				
				int num = Integer.parseInt(linea[9]);
				int id = Integer.parseInt(linea[0]);
				VOMovingViolations nuevo = new VOMovingViolations(id, linea[2], linea[13], num, linea[12], linea[15], linea[14]);
				lista.añadirFinal(nuevo);
				linea = lector.readNext();
			}
			lector.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Ocurrio un error al leer el archivo " + e.getMessage());
		} 
	}


	@Override
	public LinkedList <VOMovingViolations> getMovingViolationsByViolationCode (String violationCode) {
		// TODO Auto-generated method stub
		LinkedList<VOMovingViolations> retornar = new LinkedList<VOMovingViolations>();
		for(VOMovingViolations e: lista)
		{
			if(e.getViolationCode().equals(violationCode))
			{
				retornar.añadirFinal(e);
			}
		}
		return retornar;
	}

	@Override
	public LinkedList <VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) {
		// TODO Auto-generated method stub
		LinkedList<VOMovingViolations> retornar = new LinkedList<VOMovingViolations>();
		for(VOMovingViolations e: lista)
		{
			if(e.getAccidentIndicator().equals(accidentIndicator))
			{
				retornar.añadirFinal(e);
			}
		}
		return retornar;
	}	


}

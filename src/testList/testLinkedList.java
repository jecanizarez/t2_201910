package testList;
import java.util.Iterator;

import org.junit.*;

import com.sun.scenario.effect.impl.state.LinearConvolveRenderState;

import junit.framework.TestCase;
import model.data_structures.LinkedList;

public class testLinkedList extends TestCase {
	
	
	private LinkedList<String> listaStrings;
	
	private void setupEscenario1()
	{
		listaStrings = new LinkedList<String>(); 
		listaStrings.a�adirFinal("Hola");
		listaStrings.a�adirFinal("Como Estas"); 
		listaStrings.a�adirFinal("Bien");
	}
	
	public void testA�adir()
	{
		setupEscenario1();
		assertEquals("Error al a�adir los objetos", 3, listaStrings.getSize().intValue());
	}
	public void testEliminarUlt()
	{
		setupEscenario1();
		listaStrings.eliminarUlt();
		assertEquals("Error al eliminar un objeto", 2, listaStrings.getSize().intValue());
	}
	public void testGetElement()
	{
		setupEscenario1();
		assertEquals("Error al buscar un elemento en la posicion indicada", "Hola", listaStrings.getElement(0));
		
	}
	public void testGetCurrentElement()
	{
		setupEscenario1();
		assertEquals("Error al buscar el elemento actual(primero)", "Hola", listaStrings.getCurrentElement());
	}
	public void testA�adirAt()
	{
		setupEscenario1();
		listaStrings.a�adirAt(2, "Espera");
		assertEquals("Error al a�adir el objeto", 4, listaStrings.getSize().intValue());
		assertEquals("Error al a�adir el objeto","Espera",listaStrings.getElement(2));

	}
	public void testEliminarAt()
	{
		setupEscenario1();
		listaStrings.EliminarAt(0);
		for(String e: listaStrings)
		{
			System.out.println(e);
		}
		assertEquals("Error al eliminar el objeto en la posicion indicada",2,listaStrings.getSize().intValue());
		
	}
	
	
	
	
	
	
	

}
